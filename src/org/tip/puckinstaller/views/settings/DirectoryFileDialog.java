/*
 * Copyright 2015-2017 Christian Pierre MOMON, DEVINSY, TIP, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of PuckInstaller. This software (PuckInstaller) is a
 * computer program whose purpose is to install PUCK (Program for the Use and
 * Computation of Kinship data), an open interactive platform for archiving,
 * sharing, analyzing and comparing kinship data used in scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckinstaller.views.settings;

import java.awt.Component;
import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DirectoryFileDialog.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class DirectoryFileDialog extends FileDialog
{
	private static final long serialVersionUID = 4540106072398021101L;
	private static Logger logger = LoggerFactory.getLogger(DirectoryFileDialog.class);

	/**
	 * Instantiates a new directory file dialog.
	 * 
	 * @param title
	 *            the title
	 * @param targetDirectory
	 *            the target directory
	 */
	public DirectoryFileDialog(final String title, final File targetDirectory)
	{
		super((Frame) null, title);

		setMode(FileDialog.LOAD);
		if (targetDirectory != null)
		{
			setDirectory(targetDirectory.getAbsolutePath());
		}
		setFilenameFilter(new DirectoryFilenameFilter());
		setVisible(true);
	}

	/**
	 * Show selector dialog.
	 * 
	 * @param parent
	 *            the parent
	 * @param title
	 *            the title
	 * @param targetFile
	 *            the target file
	 * @return the file
	 */
	public static File showSelectorDialog(final Component parent, final String title, final File targetFile)
	{
		File result;

		System.setProperty("apple.awt.fileDialogForDirectories", "true");
		DirectoryFileDialog selector = new DirectoryFileDialog(title, targetFile);

		String directory = selector.getDirectory();
		String filename = selector.getFile();
		logger.debug("result=[directory={}][filename={}]", directory, filename);
		if (StringUtils.isBlank(directory))
		{
			result = null;
		}
		else if (StringUtils.isBlank(filename))
		{
			result = new File(directory);
		}
		else
		{
			result = new File(directory, filename);
		}
		logger.debug("result=[{}]", result);

		//
		return result;
	}
}
