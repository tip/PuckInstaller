/*
 * Copyright 2015-2017 Christian Pierre MOMON, DEVINSY, TIP, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of PuckInstaller. This software (PuckInstaller) is a
 * computer program whose purpose is to install PUCK (Program for the Use and
 * Computation of Kinship data), an open interactive platform for archiving,
 * sharing, analyzing and comparing kinship data used in scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckinstaller.views.settings;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DirectoryFileChooser is based on the dialog type so it must set at
 * first.
 * 
 * http://stackoverflow.com/questions/596429/adjust-selected-file-to-filefilter-
 * in-a-jfilechooser
 * 
 * @author cpm
 */
public class DirectoryFileChooser extends JFileChooser
{
	private static final long serialVersionUID = -2579851322257142775L;
	private static final Logger logger = LoggerFactory.getLogger(DirectoryFileChooser.class);

	/**
	 * Instantiates a new directory file chooser.
	 * 
	 * @param title
	 *            the title
	 * @param targetFile
	 *            the target file
	 */
	public DirectoryFileChooser(final String title, final File targetFile)
	{
		super();

		//
		setSelectedFile(targetFile);
		setDialogTitle(title);
		setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		setAcceptAllFileFilterUsed(false);
		setApproveButtonText("Select");
		setDialogType(CUSTOM_DIALOG);
	}

	/* (non-Javadoc)
	 * @see javax.swing.JFileChooser#approveSelection()
	 */
	@Override
	public void approveSelection()
	{
		File targetFile = getSelectedFile();

		logger.debug("getCurrentDirectory(): {}", getCurrentDirectory());
		logger.debug("selectedFile={}", targetFile);

		if ((targetFile.exists()) && (targetFile.isDirectory()))
		{
			super.approveSelection();
		}
		else
		{
			super.approveSelection();
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.JFileChooser#cancelSelection()
	 */
	@Override
	public void cancelSelection()
	{
		logger.debug("Cancel selection.");
		super.cancelSelection();
	}

	/* (non-Javadoc)
	 * @see javax.swing.JFileChooser#setSelectedFile(java.io.File)
	 */
	@Override
	public void setSelectedFile(final File file)
	{
		super.setSelectedFile(file);
	}

	/* (non-Javadoc)
	 * @see javax.swing.JComponent#setVisible(boolean)
	 */
	@Override
	public void setVisible(final boolean visible)
	{
		super.setVisible(visible);

		if (!visible)
		{
			resetChoosableFileFilters();
		}
	}

	/**
	 * This method is the main one of the selector.
	 * 
	 * @param parent
	 *            the parent
	 * @param title
	 *            the title
	 * @param targetFile
	 *            the target file
	 * @return the file
	 */
	public static File showSelectorDialog(final Component parent, final String title, final File targetFile)
	{
		File result;

		DirectoryFileChooser selector = new DirectoryFileChooser(title, targetFile);

		if (selector.showDialog(parent, null) == JFileChooser.APPROVE_OPTION)
		{
			logger.debug("getCurrentDirectory(): {}", selector.getCurrentDirectory());
			logger.debug("getSelectedFile() : {}", selector.getSelectedFile());
			result = selector.getSelectedFile();
		}
		else
		{
			result = null;
		}

		//
		return result;
	}
}
