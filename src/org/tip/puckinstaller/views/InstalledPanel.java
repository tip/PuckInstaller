/*
 * Copyright 2015-2017 Christian Pierre MOMON, DEVINSY, TIP, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of PuckInstaller. This software (PuckInstaller) is a
 * computer program whose purpose is to install PUCK (Program for the Use and
 * Computation of Kinship data), an open interactive platform for archiving,
 * sharing, analyzing and comparing kinship data used in scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckinstaller.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.io.File;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.tip.puckinstaller.model.InstallCriteria;

/**
 * The Class InstalledPanel.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class InstalledPanel extends JPanel
{
	private static final long serialVersionUID = -5877824042756428263L;

	/**
	 * Create the panel.
	 * 
	 * @param criteria
	 *            the criteria
	 */
	public InstalledPanel(final InstallCriteria criteria)
	{
		setLayout(new BorderLayout(0, 0));

		JLabel lblInstallDone = new JLabel("Install done");
		lblInstallDone.setFont(new Font("Dialog", Font.BOLD, 32));
		add(lblInstallDone, BorderLayout.NORTH);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		add(horizontalStrut_1, BorderLayout.WEST);

		JEditorPane txtpnPuckIsInstalled = new JEditorPane("text/html", "");
		txtpnPuckIsInstalled.setEditable(false);
		txtpnPuckIsInstalled
				.setText("<html>\n<p>Congratulation! PUCK is installed.</p>\n\n<p>Now, go in the <i>PuckBin</i> directory:</p>\n\n<p><b>$PUCKBIN</b></p>\n\n<p>and run <b><$PUCKLAUNCHERFILE</b> to launch PUCK.</p>\n\n<p>The <b>$PUCKLAUNCHERFILE</b> file is available for copy, link and shortcut everywhere you want it.</p>\n</html>");
		add(txtpnPuckIsInstalled, BorderLayout.CENTER);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		add(horizontalStrut_2, BorderLayout.EAST);

		JPanel panel = new JPanel();
		add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		Component verticalStrut = Box.createVerticalStrut(20);
		panel.add(verticalStrut);

		// ///////////////////
		String text = txtpnPuckIsInstalled.getText();
		text = text.replace("$PUCKBIN", criteria.getInstallDirectory().getAbsolutePath() + File.separatorChar);
		if (criteria.isGNULinux())
		{
			text = text.replace("$PUCKLAUNCHERFILE", "puck.sh");
		}
		else if (criteria.isOSX())
		{
			text = text.replace("$PUCKLAUNCHERFILE", "puck or puck.command");
		}
		else if (criteria.isMSWindows())
		{
			text = text.replace("$PUCKLAUNCHERFILE", "puck or puck.bat");
		}
		txtpnPuckIsInstalled.setText(text);
	}
}
