/**
 * Copyright 2015-2017 Christian Pierre MOMON, DEVINSY, TIP, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of PuckInstaller. This software (PuckInstaller) is a
 * computer program whose purpose is to install PUCK (Program for the Use and
 * Computation of Kinship data), an open interactive platform for archiving,
 * sharing, analyzing and comparing kinship data used in scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckinstaller.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Font;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import fr.devinsy.util.FileTools;

/**
 * The Class WelcomePanel.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class WelcomePanel extends JPanel
{
	private static final long serialVersionUID = 7820156017631003040L;

	/**
	 * Instantiates a new welcome panel.
	 */
	public WelcomePanel()
	{
		setLayout(new BorderLayout(0, 0));

		JPanel panel_1 = new JPanel();
		add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));

		JLabel lblWelcome = new JLabel("WELCOME");
		panel_1.add(lblWelcome);
		lblWelcome.setFont(new Font("Dialog", Font.BOLD, 32));

		JEditorPane dtrpnText = new JEditorPane("text/html", "");
		dtrpnText.setEditorKit(JEditorPane.createEditorKitForContentType("text/html"));
		dtrpnText.addHyperlinkListener(new HyperlinkListener()
		{
			@Override
			public void hyperlinkUpdate(final HyperlinkEvent event)
			{
				try
				{
					if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
					{
						// Do something with e.getURL() here
						if (Desktop.isDesktopSupported())
						{
							//
							Desktop desktop = Desktop.getDesktop();
							if (desktop.isSupported(Desktop.Action.BROWSE))
							{
								//
								desktop.browse(event.getURL().toURI());
							}
						}
						else
						{
							String title = "Error";
							String message = "Sorry, no browser seems to be available.";
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				catch (java.io.IOException ioe)
				{
					ioe.printStackTrace();
					System.err.println("The system cannot find the URL specified: [" + event.getURL().toString() + "]");
					String title = "Error";
					String message = "Sorry, browser call returns an error.";
					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
				}
				catch (java.net.URISyntaxException use)
				{
					use.printStackTrace();
					System.out.println("Illegal character in path [" + event.getURL().toString() + "]");
					String title = "Error";
					String message = "Sorry, browser call returns an error.";
					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		dtrpnText.setEditable(false);
		String html;
		try
		{
			html = FileTools.load(WelcomePanel.class.getResource("/org/tip/puckinstaller/views/welcome.html"));
		}
		catch (IOException exception)
		{
			exception.printStackTrace();
			html = "<html>Error reading welcome file.</html>";
		}

		// dtrpnText
		// .setText("<html>\n<p>To enjoy the best experience with PUCK, a specific environment is required.</p>\n\n<p>The PUCK Installer will take care about it for you.</p>\n\n<p>\nPUCK is free software under CeCILL licence version 2 (http://www.cecill.info/).<br/>\nPUCKInstaller is free software under CeCILL licence version 2 (http://www.cecill.info/).\n</p>\n\n<p>\nReport issue in <a href=\"https://www.kinsources.net/agora/forum.xhtml?forum_id=4\">PUCK dedicated forum</a> hosted on <a href=\"https://www.kinsources.net/\">https://www.kinsoures.net/</a>.\n</p>\n\n</html>");
		dtrpnText.setText(html);
		add(dtrpnText);

		JPanel panel = new JPanel();
		add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		Component verticalStrut = Box.createVerticalStrut(20);
		panel.add(verticalStrut);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		add(horizontalStrut, BorderLayout.WEST);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		add(horizontalStrut_1, BorderLayout.EAST);
	}
}
