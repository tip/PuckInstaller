/*
 * Copyright 2015-2017 Christian Pierre MOMON, DEVINSY, TIP, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of PuckInstaller. This software (PuckInstaller) is a
 * computer program whose purpose is to install PUCK (Program for the Use and
 * Computation of Kinship data), an open interactive platform for archiving,
 * sharing, analyzing and comparing kinship data used in scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckinstaller.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puckinstaller.PuckInstallerException;
import org.tip.puckinstaller.model.InstallCriteria;
import org.tip.puckinstaller.model.PuckInstallerManager;
import org.tip.puckinstaller.model.PuckRepository;

/**
 * The Class InstallingPanel.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */

public class InstallingPanel extends JPanel
{
	private static final long serialVersionUID = -2596794252517096353L;
	private static final Logger logger = LoggerFactory.getLogger(InstallingPanel.class);

	private InstallCriteria criteria;
	private JTextArea txtrConsole;

	/**
	 * Create the panel.
	 * 
	 * @param criteria
	 *            the criteria
	 */
	public InstallingPanel(final InstallCriteria criteria)
	{
		this.criteria = criteria;

		// //////////////////////////

		setLayout(new BorderLayout(0, 0));

		JLabel lblInstalling = new JLabel("Installing");
		add(lblInstalling, BorderLayout.NORTH);
		lblInstalling.setFont(new Font("Dialog", Font.BOLD, 32));

		Component horizontalStrut = Box.createHorizontalStrut(20);
		add(horizontalStrut, BorderLayout.WEST);

		this.txtrConsole = new JTextArea();
		this.txtrConsole.setFont(new Font("Monospaced", Font.PLAIN, 13));
		this.txtrConsole.setEditable(false);
		((DefaultCaret) this.txtrConsole.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		JScrollPane scrollPane = new JScrollPane(this.txtrConsole);
		add(scrollPane);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		add(horizontalStrut_1, BorderLayout.EAST);

		JPanel panel = new JPanel();
		add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		Component verticalStrut = Box.createVerticalStrut(20);
		panel.add(verticalStrut);

		// /////////////////////
	}

	/**
	 * Append pad.
	 * 
	 * @param text
	 *            the text
	 */
	private void appendPad(final String text)
	{
		this.txtrConsole.append(StringUtils.rightPad(text, 40, '.'));
	}

	/**
	 * Install.
	 * 
	 * @throws PuckInstallerException
	 *             the puck installer exception
	 */
	public void install() throws PuckInstallerException
	{
		try
		{
			this.txtrConsole.append("Install starting…\n");
			this.txtrConsole.append("\n");

			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException exception)
			{
				exception.printStackTrace();
			}

			//
			{
				appendPad("Creation of the install directory");

				if (this.criteria.getInstallDirectory().exists())
				{
					this.txtrConsole.append("PASSED (already existing).\n");
				}
				else
				{
					this.criteria.getInstallDirectory().mkdirs();
					this.txtrConsole.append("DONE\n");
				}
			}

			//
			if (this.criteria.getJreRepository() != null)
			{
				//
				appendPad("Downloading the JRE zip file");
				File jreLocalZip = PuckRepository.dowloadJre(this.criteria.getJreRepository(), this.criteria.getOperatingSystem(), this.criteria.getInstallDirectory());
				this.txtrConsole.append("DONE\n");

				//
				appendPad("Unziping the JRE zip file");
				PuckRepository.unzip(jreLocalZip, this.criteria.getInstallDirectory());
				this.txtrConsole.append("DONE\n");

				//
				appendPad("Removing JRE zip file");
				jreLocalZip.delete();
				this.txtrConsole.append("DONE\n");

				//
				if (!SystemUtils.IS_OS_WINDOWS)
				{
					appendPad("Setting executable flag for JRE");
					File javaFile = new File(this.criteria.buildJreHome(), "bin/java");
					javaFile.setExecutable(true, true);
					this.txtrConsole.append("DONE\n");
				}
			}

			//
			appendPad("Downloading the Puck zip file");
			File puckLocalZip = PuckRepository.dowloadPuckPackage(this.criteria.getPuckRepository(), this.criteria.getPuckPackage(), this.criteria.getInstallDirectory());
			this.txtrConsole.append("DONE\n");

			//
			appendPad("Unziping the Puck zip file");
			PuckRepository.unzip(puckLocalZip, this.criteria.getInstallDirectory());
			this.txtrConsole.append("DONE\n");

			//
			appendPad("Removing Puck zip file");
			puckLocalZip.delete();
			this.txtrConsole.append("DONE\n");

			//
			appendPad("Creating icon file");
			PuckInstallerManager.createIconFile(this.criteria.getInstallDirectory());
			this.txtrConsole.append("DONE\n");

			//
			appendPad("Creating launcher file");
			PuckInstallerManager.createLauncherScript(this.criteria.getInstallDirectory(), this.criteria.buildJreBin(), this.criteria.buildPuckPackageHome(), this.criteria.getOperatingSystem(),
					puckLocalZip.getName(), this.criteria.getMemory());
			this.txtrConsole.append("DONE\n");

			//
			appendPad("Creating desktop file");
			PuckInstallerManager.createDesktopFile(this.criteria.getInstallDirectory(), this.criteria.buildJreBin(), this.criteria.buildPuckPackageHome(), this.criteria.getOperatingSystem(),
					puckLocalZip.getName(), this.criteria.getMemory());
			this.txtrConsole.append("DONE\n");

			//
			if (SystemUtils.IS_OS_LINUX)
			{
				appendPad("Copying local application file");
				PuckInstallerManager.copyLocalApplicationFile(this.criteria.getInstallDirectory());
				this.txtrConsole.append("DONE\n");
			}

			//
			this.txtrConsole.append("\n");
			this.txtrConsole.append("Install done.\n");
		}
		catch (MalformedURLException exception)
		{
			logger.error("Error during installation", exception);
			this.txtrConsole.append("Sorry, error detected, task stopped.\n");
			this.txtrConsole.append(exception.getMessage());
			throw new PuckInstallerException("Error during install.", exception);
		}
		catch (IOException exception)
		{
			logger.error("Error during installation", exception);
			this.txtrConsole.append("Sorry, error detected. Task stopped.\n");
			this.txtrConsole.append(exception.getMessage());
			throw new PuckInstallerException("Error during install.", exception);
		}
		catch (Exception exception)
		{
			logger.error("Error during installation", exception);
			this.txtrConsole.append("Sorry, error detected. Task stopped.\n");
			this.txtrConsole.append(exception.getMessage());
			throw new PuckInstallerException("Error during install.", exception);
		}
	}
}
