/*
 * Copyright 2015-2017 Christian Pierre MOMON, DEVINSY, TIP, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of PuckInstaller. This software (PuckInstaller) is a
 * computer program whose purpose is to install PUCK (Program for the Use and
 * Computation of Kinship data), an open interactive platform for archiving,
 * sharing, analyzing and comparing kinship data used in scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckinstaller.model;

import java.text.Collator;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class NormalizedPuckPackageComparator implements Comparator<String>
{
	private static Pattern VERSION_PATTERN = Pattern.compile("^\\D+(\\d+)\\D(\\d+)\\D(\\d+.*)$");

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(final String alpha, final String bravo)
	{
		int result;

		//
		String alphaLabel;
		if (alpha == null)
		{
			alphaLabel = null;
		}
		else
		{
			Matcher matcher = VERSION_PATTERN.matcher(alpha);
			if (((matcher.find()) && (matcher.groupCount() == 3)))
			{
				alphaLabel = String.format("%s-%s-%s", StringUtils.leftPad(matcher.group(1), 3, '0'), StringUtils.leftPad(matcher.group(2), 3, '0'), StringUtils.leftPad(matcher.group(3), 3, '0'));
			}
			else
			{
				alphaLabel = null;
			}
		}

		//
		String bravoLabel;
		if (bravo == null)
		{
			bravoLabel = null;
		}
		else
		{
			Matcher matcher = VERSION_PATTERN.matcher(bravo);
			if (((matcher.find()) && (matcher.groupCount() == 3)))
			{
				bravoLabel = String.format("%s-%s-%s", StringUtils.leftPad(matcher.group(1), 3, '0'), StringUtils.leftPad(matcher.group(2), 3, '0'), StringUtils.leftPad(matcher.group(3), 3, '0'));
			}
			else
			{
				bravoLabel = null;
			}
		}

		//
		result = Collator.getInstance().compare(alphaLabel, bravoLabel);
		System.out.println("yop:" + alphaLabel + " " + bravoLabel + " " + result);

		//
		return result;
	}
}
