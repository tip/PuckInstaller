/*
 * Copyright 2015-2017 Christian Pierre MOMON, DEVINSY, TIP, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of PuckInstaller. This software (PuckInstaller) is a
 * computer program whose purpose is to install PUCK (Program for the Use and
 * Computation of Kinship data), an open interactive platform for archiving,
 * sharing, analyzing and comparing kinship data used in scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckinstaller.model;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puckinstaller.model.InstallCriteria.OperatingSystem;
import org.tip.puckinstaller.views.InstallingPanel;

import fr.devinsy.util.cmdexec.CmdExec;
import fr.devinsy.util.cmdexec.CmdExecException;
import fr.devinsy.util.strings.StringList;

/**
 * The Class PuckInstallerManager.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class PuckInstallerManager
{
	private static final Logger logger = LoggerFactory.getLogger(PuckInstallerManager.class);

	/**
	 * Instantiates a new puck installer manager.
	 */
	private PuckInstallerManager()
	{
	}

	/**
	 * Copy local application file.
	 * 
	 * @param targetDirectory
	 *            the target directory
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void copyLocalApplicationFile(final File targetDirectory) throws IOException
	{
		File localShareApplicationDirectory = new File(SystemUtils.getUserHome(), ".local/share/applications/");
		localShareApplicationDirectory.mkdirs();

		File source = new File(targetDirectory, "puck.desktop");
		File target = new File(SystemUtils.getUserHome(), ".local/share/applications/puck.desktop");

		FileUtils.copyFile(source, target);
	}

	/**
	 * Creates the desktop file.
	 * 
	 * @param targetDirectory
	 *            the target directory
	 * @param jreBin
	 *            the jre bin
	 * @param puckPackageHome
	 *            the puck package home
	 * @param operatingSystem
	 *            the operating system
	 * @param puckPackage
	 *            the puck package
	 * @param memory
	 *            the memory
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws CmdExecException
	 *             the cmd exec exception
	 */
	public static void createDesktopFile(final File targetDirectory, final File jreBin, final File puckPackageHome, final OperatingSystem operatingSystem, final String puckPackage, final int memory)
			throws IOException, CmdExecException
	{
		logger.debug("jreBin=[{}]", jreBin);

		if ((operatingSystem == OperatingSystem.GNULinux_32) || (operatingSystem == OperatingSystem.GNULinux_64))
		{
			//
			StringList desktop = new StringList();

			desktop.appendln("[Desktop Entry]");
			desktop.appendln("Name=Puck");
			desktop.appendln("GenericName=Puck");
			desktop.appendln("Comment=Program for the Use and Computation of Kinship data");
			desktop.appendln("Icon=" + targetDirectory.getAbsolutePath() + File.separatorChar + "puck.jpg");
			desktop.appendln("Type=Application");
			desktop.appendln("Categories=Science");
			desktop.appendln("Exec=/bin/sh \"" + targetDirectory.getAbsolutePath() + File.separatorChar + "puck.sh\" %F");
			desktop.appendln("StartupNotify=false");
			desktop.appendln("Terminal=false");
			desktop.appendln("MimeType=application/puc;");
			desktop.appendln("X-Desktop-File-Install-Version=0.21");

			// desktop.appendln("\"" + jreHome + "bin/java\" -Xms368m -Xmx" +
			// memory + "m -jar \"" + puckPackageHome + "puck.jar\" $@");

			//
			File desktopFile = new File(targetDirectory, "puck.desktop");
			FileUtils.write(desktopFile, desktop.toString());
			desktopFile.setExecutable(true, true);
		}
		else if ((operatingSystem == OperatingSystem.MSWindows_32) || (operatingSystem == OperatingSystem.MSWindows_64))
		{
			File linkFile = new File(targetDirectory, "puck.lnk");
			String jreBinPath;
			if (jreBin == null)
			{
				jreBinPath = "";
			}
			else
			{
				jreBinPath = jreBin.getAbsolutePath() + File.separatorChar;
			}
			String arguments = "-Xms368m -Xmx" + memory + "m -jar \"\"" + puckPackageHome + File.separatorChar + "puck.jar\"\"";
			File iconFile = new File(targetDirectory, "puck.ico");
			String description = "Program for the Use and Computation of Kinship data";

			StringList script = new StringList();
			script.appendln("Set oWS = WScript.CreateObject(\"WScript.Shell\")");
			script.appendln("sLinkFile = \"" + linkFile.getAbsolutePath() + "\"");
			script.appendln("Set oLink = oWS.CreateShortcut(sLinkFile)");
			script.appendln("oLink.TargetPath = \"" + jreBinPath + "javaw.exe\"");
			script.appendln("oLink.Arguments = \"" + arguments + "\"");
			script.appendln("oLink.Description = \"" + description + "\"");
			// script.appendln("oLink.HotKey = \"ALT+CTRL+F\"");
			script.appendln("oLink.IconLocation = \"" + iconFile.getAbsolutePath() + "\"");
			script.appendln("oLink.WindowStyle = \"1\"");
			// script.appendln("oLink.WorkingDirectory = \"C:\\Program Files\\MyApp\"");
			script.appendln("oLink.Save");

			File linkMakerFile = new File(targetDirectory, "linkMaker.vbs");
			FileUtils.writeLines(linkMakerFile, script);

			CmdExec.run("wscript \"" + linkMakerFile.getAbsolutePath() + "\"");

			linkMakerFile.delete();
		}
		else if ((operatingSystem == OperatingSystem.OSX_32) || (operatingSystem == OperatingSystem.OSX_64))
		{
			String arguments = "-Xms368m -Xmx" + memory + "m -jar '" + puckPackageHome + File.separatorChar + "puck.jar'";

			//
			StringList script = new StringList();
			script.append("do shell script \"'");
			if (jreBin != null)
			{
				script.append(jreBin.getAbsolutePath());
				script.append(File.separatorChar);
			}
			script.append("java");
			script.append("' ");
			script.append(arguments);
			script.appendln("\"");
			script.appendln("tell application \"X11\" to quit");

			//
			File scriptFile = new File(targetDirectory, "puck.script");
			FileUtils.write(scriptFile, script.toString());
			logger.debug("script={}", script);

			//
			File applicationFile = new File(targetDirectory, "puck.app");
			StringList command = new StringList();
			command.append("osacompile");
			command.append("-o");
			command.append(applicationFile.getAbsolutePath());
			command.append(scriptFile.getAbsolutePath());
			CmdExec.run(command.toStringArray());
			scriptFile.delete();

			//
			URL source = InstallingPanel.class.getResource("/org/tip/puckinstaller/logo-puck-128x.icns");
			File iconFile = new File(applicationFile, "Contents/Resources/applet.icns");
			FileUtils.copyURLToFile(source, iconFile);
		}
	}

	/**
	 * Creates the icon file.
	 * 
	 * @param targetDirectory
	 *            the target directory
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void createIconFile(final File targetDirectory) throws IOException
	{
		if (SystemUtils.IS_OS_WINDOWS)
		{
			URL source = InstallingPanel.class.getResource("/org/tip/puckinstaller/logo-puck-250x.ico");
			File target = new File(targetDirectory, "puck.ico");
			FileUtils.copyURLToFile(source, target);
		}
		else if (SystemUtils.IS_OS_LINUX)
		{
			URL source = InstallingPanel.class.getResource("/org/tip/puckinstaller/logo-puck-250x.jpg");
			File target = new File(targetDirectory, "puck.jpg");
			FileUtils.copyURLToFile(source, target);
		}
	}

	/**
	 * Creates the launcher script.
	 * 
	 * @param targetDirectory
	 *            the target directory
	 * @param jreBin
	 *            the jre bin
	 * @param puckPackageHome
	 *            the puck package home
	 * @param operatingSystem
	 *            the operating system
	 * @param puckPackage
	 *            the puck package
	 * @param memory
	 *            the memory
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void createLauncherScript(final File targetDirectory, final File jreBin, final File puckPackageHome, final OperatingSystem operatingSystem, final String puckPackage, final int memory)
			throws IOException
	{
		logger.debug("jreBin=[{}]", jreBin);

		if ((operatingSystem == OperatingSystem.GNULinux_32) || (operatingSystem == OperatingSystem.GNULinux_64))
		{
			//
			StringList script = new StringList();
			script.appendln("#!/bin/sh");
			script.append("\"");
			if (jreBin != null)
			{
				script.append(jreBin.getAbsolutePath());
				script.append(File.separatorChar);
			}
			script.append("java\" -Xms368m -Xmx");
			script.append(memory);
			script.append("m -jar \"");
			script.append(puckPackageHome.getAbsolutePath());
			script.append(File.separatorChar);
			script.appendln("puck.jar\" $@");

			//
			File scriptFile = new File(targetDirectory, "puck.sh");
			FileUtils.write(scriptFile, script.toString());
			scriptFile.setExecutable(true, true);
		}
		else if ((operatingSystem == OperatingSystem.OSX_32) || (operatingSystem == OperatingSystem.OSX_64))
		{
			//
			StringList script = new StringList();
			script.appendln("#!/bin/sh");
			script.append("\"");
			if (jreBin != null)
			{
				script.append(jreBin.getAbsolutePath());
				script.append(File.separatorChar);
			}
			script.append("java\" -Xms368m -Xmx");
			script.append(memory);
			script.append("m -jar \"");
			script.append(puckPackageHome.getAbsolutePath());
			script.append(File.separatorChar);
			script.appendln("puck.jar\" $@");

			//
			File scriptFile = new File(targetDirectory, "puck.command");
			FileUtils.write(scriptFile, script.toString());
			scriptFile.setExecutable(true, true);
		}
		else if ((operatingSystem == OperatingSystem.MSWindows_32) || (operatingSystem == OperatingSystem.MSWindows_64))
		{
			//
			StringList script = new StringList();
			script.append("start /b \"\"");
			script.append(" \"");
			if (jreBin != null)
			{
				script.append(jreBin.getAbsolutePath());
				script.append(File.separatorChar);
			}
			script.append("javaw.exe\"");
			script.append(" -Xms368m -Xmx");
			script.append(memory);
			script.append("m -jar \"");
			script.append(puckPackageHome.getAbsolutePath());
			script.append(File.separatorChar);
			script.appendln("puck.jar\" %1 %2 %3 %4 %5 %6 %7");

			//
			File scriptFile = new File(targetDirectory, "puck.bat");
			FileUtils.write(scriptFile, script.toString());
		}
		else
		{
			logger.warn("Undefined operatingSystem value [{}]", operatingSystem);
		}
	}

	/**
	 * This method computes the available memory of the system.
	 * 
	 * @return the available system memory KB
	 */
	public static int getAvailableSystemMemoryKB()
	{
		int result;

		try
		{
			if (SystemUtils.IS_OS_LINUX)
			{
				StringList lines = new StringList(FileUtils.readLines(new File("/proc/meminfo")));

				String[] line = lines.get(2).split(" ");
				result = Integer.parseInt(line[line.length - 2]);
			}
			else
			{
				com.sun.management.OperatingSystemMXBean os = (com.sun.management.OperatingSystemMXBean) java.lang.management.ManagementFactory.getOperatingSystemMXBean();
				long freeMemorySize = os.getFreePhysicalMemorySize();

				result = (int) (freeMemorySize / 1024);
			}
		}
		catch (IOException exception)
		{
			exception.printStackTrace();
			result = 0;
		}

		logger.debug("availableMemorySize=" + result);

		//
		return result;
	}

	/**
	 * Gets the system physical memory KB.
	 * 
	 * @return the system physical memory KB
	 */
	public static int getSystemPhysicalMemoryKB()
	{
		int result;

		try
		{
			if (SystemUtils.IS_OS_LINUX)
			{
				StringList lines = new StringList(FileUtils.readLines(new File("/proc/meminfo")));
				String[] firstLine = lines.get(0).split(" ");
				result = Integer.parseInt(firstLine[firstLine.length - 2]);
			}
			else
			{
				com.sun.management.OperatingSystemMXBean os = (com.sun.management.OperatingSystemMXBean) java.lang.management.ManagementFactory.getOperatingSystemMXBean();
				long physicalMemorySize = os.getTotalPhysicalMemorySize();
				logger.debug("physicalMemorySize=" + physicalMemorySize);
				result = (int) (physicalMemorySize / 1024);
			}
		}
		catch (IOException exception)
		{
			exception.printStackTrace();
			result = 0;
		}

		//
		return result;
	}

	/**
	 * Checks if is 32 bits.
	 * 
	 * @return true, if is 32 bits
	 */
	public static boolean is32bits()
	{
		boolean result;

		String arch = System.getProperty("sun.arch.data.model");

		if (arch == null)
		{
			result = true;
		}
		else if (arch.contains("32"))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}
}
