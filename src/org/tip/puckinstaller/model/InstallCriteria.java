/*
 * Copyright 2015-2017 Christian Pierre MOMON, DEVINSY, TIP, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of PuckInstaller. This software (PuckInstaller) is a
 * computer program whose purpose is to install PUCK (Program for the Use and
 * Computation of Kinship data), an open interactive platform for archiving,
 * sharing, analyzing and comparing kinship data used in scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckinstaller.model;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class InstallCriteria.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class InstallCriteria
{
	public enum OperatingSystem
	{
		GNULinux_32,
		GNULinux_64,
		MSWindows_32,
		MSWindows_64,
		OSX_32,
		OSX_64
	}

	private static final Logger logger = LoggerFactory.getLogger(InstallCriteria.class);

	public static final String DEFAULT_REPOSITORY_PATH = "http://www.devinsy.fr/Puck/Repository/";
	public static final String DEFAULT_INSTALL_PATH_MSWINDOWS = "C:\\PROGRAM FILES\\PuckBin\\";
	public static final int DEFAULT_MEMORY_MB = 1024;

	private String jreRepository;
	private String puckRepository;
	private String puckPackage;
	private File installDirectory;
	private OperatingSystem operatingSystem;
	private int memory;
	private File systemJreHome;

	/**
	 * Instantiates a new install criteria.
	 */
	public InstallCriteria()
	{
		this.jreRepository = DEFAULT_REPOSITORY_PATH;
		this.puckRepository = DEFAULT_REPOSITORY_PATH;
		this.puckPackage = null;
		this.installDirectory = getDefaultInstallDirectory();
		this.operatingSystem = null;
		this.memory = DEFAULT_MEMORY_MB;
	}

	/**
	 * Builds the jre bin.
	 * 
	 * @return the file
	 */
	public File buildJreBin()
	{
		File result;

		File jreHome = buildJreHome();
		if (jreHome == null)
		{
			result = null;
		}
		else
		{
			result = new File(jreHome, "bin");
		}

		//
		return result;
	}

	/**
	 * Builds the jre home.
	 * 
	 * @return the file
	 */
	public File buildJreHome()
	{
		File result;

		if ((this.operatingSystem == OperatingSystem.OSX_32) || (this.operatingSystem == OperatingSystem.OSX_64))
		{
			if (this.systemJreHome == null)
			{
				result = null;
			}
			else
			{
				result = this.systemJreHome;
			}
		}
		else
		{
			if (StringUtils.isBlank(this.jreRepository))
			{
				result = null;
			}
			else
			{
				switch (this.operatingSystem)
				{
					case GNULinux_32:
						result = new File(this.installDirectory, PuckRepository.GNULINUX_32_JRE_FILENAME.replace(".zip", SystemUtils.FILE_SEPARATOR));
					break;

					case GNULinux_64:
						result = new File(this.installDirectory, PuckRepository.GNULINUX_64_JRE_FILENAME.replace(".zip", SystemUtils.FILE_SEPARATOR));
					break;

					case MSWindows_32:
						result = new File(this.installDirectory, PuckRepository.MSWINDOWS_32_JRE_FILENAME.replace(".zip", SystemUtils.FILE_SEPARATOR));
					break;

					case MSWindows_64:
						result = new File(this.installDirectory, PuckRepository.MSWINDOWS_64_JRE_FILENAME.replace(".zip", SystemUtils.FILE_SEPARATOR));
					break;

					case OSX_32:
					case OSX_64:
						result = this.systemJreHome;
					break;

					default:
						result = null;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Builds the puck package home.
	 * 
	 * @return the file
	 */
	public File buildPuckPackageHome()
	{
		File result;

		result = new File(this.installDirectory, this.puckPackage);

		//
		return result;
	}

	/**
	 * Gets the install directory.
	 * 
	 * @return the install directory
	 */
	public File getInstallDirectory()
	{
		return this.installDirectory;
	}

	/**
	 * Gets the jre repository.
	 * 
	 * @return the jre repository
	 */
	public String getJreRepository()
	{
		return this.jreRepository;
	}

	/**
	 * Gets the memory.
	 * 
	 * @return the memory
	 */
	public int getMemory()
	{
		return this.memory;
	}

	/**
	 * Gets the operating system.
	 * 
	 * @return the operating system
	 */
	public OperatingSystem getOperatingSystem()
	{
		return this.operatingSystem;
	}

	/**
	 * Gets the puck package.
	 * 
	 * @return the puck package
	 */
	public String getPuckPackage()
	{
		return this.puckPackage;
	}

	/**
	 * Gets the puck repository.
	 * 
	 * @return the puck repository
	 */
	public String getPuckRepository()
	{
		return this.puckRepository;
	}

	/**
	 * Gets the system jre home.
	 * 
	 * @return the system jre home
	 */
	public File getSystemJreHome()
	{
		return this.systemJreHome;
	}

	/**
	 * Checks if is GNU linux.
	 * 
	 * @return true, if is GNU linux
	 */
	public boolean isGNULinux()
	{
		boolean result;

		if ((this.operatingSystem == OperatingSystem.GNULinux_32) || (this.operatingSystem == OperatingSystem.GNULinux_64))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * Checks if is MS windows.
	 * 
	 * @return true, if is MS windows
	 */
	public boolean isMSWindows()
	{
		boolean result;

		if ((this.operatingSystem == OperatingSystem.MSWindows_32) || (this.operatingSystem == OperatingSystem.MSWindows_64))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * Checks if is osx.
	 * 
	 * @return true, if is osx
	 */
	public boolean isOSX()
	{
		boolean result;

		if ((this.operatingSystem == OperatingSystem.OSX_32) || (this.operatingSystem == OperatingSystem.OSX_64))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * Checks if is unix.
	 * 
	 * @return true, if is unix
	 */
	public boolean isUnix()
	{
		boolean result;

		if ((isGNULinux()) || (isOSX()))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * Sets the install directory.
	 * 
	 * @param installDirectory
	 *            the new install directory
	 */
	public void setInstallDirectory(final File installDirectory)
	{
		this.installDirectory = installDirectory;
	}

	/**
	 * Sets the jre repository.
	 * 
	 * @param jreRepository
	 *            the new jre repository
	 */
	public void setJreRepository(final String jreRepository)
	{
		this.jreRepository = jreRepository;

		if (jreRepository != null)
		{
			if (jreRepository.startsWith("http"))
			{
				if (!jreRepository.endsWith("/"))
				{
					this.jreRepository = jreRepository + "/";
				}
			}
			else if (!jreRepository.endsWith(SystemUtils.FILE_SEPARATOR))
			{
				this.jreRepository = jreRepository + SystemUtils.FILE_SEPARATOR;
			}
		}
	}

	/**
	 * Sets the memory.
	 * 
	 * @param memory
	 *            the new memory
	 */
	public void setMemory(final int memory)
	{
		this.memory = memory;
	}

	/**
	 * Sets the operating system.
	 * 
	 * @param operationSystem
	 *            the new operating system
	 */
	public void setOperatingSystem(final OperatingSystem operationSystem)
	{
		this.operatingSystem = operationSystem;
	}

	/**
	 * Sets the puck package.
	 * 
	 * @param puckPackage
	 *            the new puck package
	 */
	public void setPuckPackage(final String puckPackage)
	{
		this.puckPackage = puckPackage;
	}

	/**
	 * Sets the puck repository.
	 * 
	 * @param puckRepository
	 *            the new puck repository
	 */
	public void setPuckRepository(final String puckRepository)
	{
		this.puckRepository = puckRepository;

		if (puckRepository != null)
		{
			if (puckRepository.startsWith("http"))
			{
				if (!puckRepository.endsWith("/"))
				{
					this.puckRepository = puckRepository + "/";
				}
			}
			else if (!puckRepository.endsWith(SystemUtils.FILE_SEPARATOR))
			{
				this.puckRepository = puckRepository + SystemUtils.FILE_SEPARATOR;
			}
		}
	}

	/**
	 * Sets the system jre home.
	 * 
	 * @param systemJreHome
	 *            the new system jre home
	 */
	public void setSystemJreHome(final File systemJreHome)
	{
		this.systemJreHome = systemJreHome;
	}

	/**
	 * Gets the default install directory.
	 * 
	 * @return the default install directory
	 */
	public static File getDefaultInstallDirectory()
	{
		File result;

		if (SystemUtils.IS_OS_UNIX)
		{
			result = new File(SystemUtils.getUserHome(), "PuckBin");
		}
		else if (SystemUtils.IS_OS_MAC_OSX)
		{
			result = new File(SystemUtils.getUserHome(), "PuckBin");
		}
		else if (SystemUtils.IS_OS_WINDOWS)
		{
			result = new File(SystemUtils.getUserHome(), "PuckBin");
		}
		else
		{
			result = new File(SystemUtils.getUserHome(), "PuckBin");
		}

		//
		return result;
	}

	/**
	 * Checks if is valid.
	 * 
	 * @param source
	 *            the source
	 * @return true, if is valid
	 */
	public static boolean isValid(final InstallCriteria source)
	{
		boolean result;

		result = true;

		//
		return result;
	}
}
