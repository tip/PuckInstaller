/*
 * Copyright 2013-2016,2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of PuckInstaller. This software (PuckInstaller) is a
 * computer program whose purpose is to install PUCK (Program for the Use and
 * Computation of Kinship data), an open interactive platform for archiving,
 * sharing, analyzing and comparing kinship data used in scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckinstaller.util;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class BuildInformation
{
	private static final Logger logger = LoggerFactory.getLogger(BuildInformation.class);

	private static String BUILD_INFORMATION_FILE = "/build_information.properties";

	private String productName;
	private String majorRevision;
	private String minorRevision;
	private String buildNumber;
	private String buildDate;
	private String generator;
	private String author;

	/**
	 * 
	 */
	public BuildInformation()
	{
		Properties build = new Properties();

		try
		{
			URL buildInformationFile = BuildInformation.class.getResource(BUILD_INFORMATION_FILE);

			if (buildInformationFile != null)
			{
				build.load(BuildInformation.class.getResource(BUILD_INFORMATION_FILE).openStream());
			}

			this.productName = build.getProperty("product.name", "PuckInstaller");
			this.majorRevision = build.getProperty("product.revision.major", "d");
			this.minorRevision = build.getProperty("product.revision.minor", "e");
			this.buildNumber = build.getProperty("product.revision.build", "v");
			this.buildDate = build.getProperty("product.revision.date", "today");
			this.generator = build.getProperty("product.revision.generator", "n/a");
			this.author = build.getProperty("product.revision.author", "n/a");
		}
		catch (IOException exception)
		{
			//
			logger.error("Error loading the build.properties file: " + exception.getMessage());
			logger.error(ExceptionUtils.getStackTrace(exception));

			//
			this.productName = "n/a";
			this.majorRevision = "n/a";
			this.minorRevision = "n/a";
			this.buildNumber = "n/a";
			this.buildDate = "n/a";
			this.generator = "n/a";
			this.author = "n/a";
		}
	}

	public String author()
	{
		return this.author;
	}

	public String buildDate()
	{
		return this.buildDate;
	}

	public String buildNumber()
	{
		return this.buildNumber;
	}

	public String generator()
	{
		return this.generator;
	}

	public String majorRevision()
	{
		return this.majorRevision;
	}

	public String minorRevision()
	{
		return this.minorRevision;
	}

	public String productName()
	{
		return this.productName;
	}

	/**
	 * 
	 */
	@Override
	public String toString()
	{
		String result;

		result = String.format("%s %s.%s.%s built on %s by %s", this.productName, this.majorRevision, this.minorRevision, this.buildNumber, this.buildDate, this.author);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String version()
	{
		String result;

		result = String.format("%s.%s.%s", this.majorRevision, this.minorRevision, this.buildNumber);

		//
		return result;
	}

	/**
	 * 
	 */
	public static boolean isDefined()
	{
		boolean result;

		if (BuildInformation.class.getResource(BUILD_INFORMATION_FILE) == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}
}
