# PUCKInstaller

To enjoy the best experience with PUCK, a specific environment is required.

This PUCK Installer will take care about it for you.

## PUCK

PUCK = Program for the Use and Computation of Kinship data.

PUCK is free software under CeCILL licence version 2 (http://www.cecill.info/).

Created 2007 by Klaus Hamberger

Kintip Group – Kinship and Computing (Traitement Informatique de la Parenté)

More information at http://www.kintip.net/ 

  
## License

PUCKInstaller is free software under CeCILL licence version 2 (http://www.cecill.info/).

Report issue in PUCK dedicated forum hosted at https://www.kinsoures.net/.

Create 2015 by Christian Pierre MOMON <christian.momon@devinsy.fr>

Software designed by DEVINSY

Project sources available at https://framagit.org/tip/PuckInstaller


## REQUIREMENTS

PuckInstaller requires:
- Java 1.6
- Eclipse Indigo 
